**The aim of the exercice is to the able to search among events and to print them according to display preferences**

*  Receive a post array
*  Modify Start and End value to datetime with local timezone
*  Adjust timezone RFC4646 to country/city
*  Compare start Date > post['events][key] value < end Date
*  Display key and date from post['events]
