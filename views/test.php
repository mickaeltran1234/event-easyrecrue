<?php
use Controllers\EventController;

require '../vendor/autoload.php';

echo 'Test du controller <br>';

$_POST = [
    'events' => [
        'lunch'  => 1558432800,
        'beer'   => 1558454400,
        'cinema' => 1558461600,
        'sleep'  => 1558476000,
    ],
    'start' => '2019-05-21 09:00:00',
    'end'   => '2019-05-21 22:00:00',
    'locale' => 'pt_BR'
];


$mesEvents = new EventController($_POST);

echo '<pre>';
echo  $mesEvents->search();
echo '</pre>';


