<?php


namespace Models;

require '../vendor/autoload.php';

class Event
{
    public function __construct($_events = Array()){
        // Apply provided attribute values
        foreach($_events as $_field=>$_value){
            $this->$_field = $_value;
        }
    }

    function __set($name,$value){
        if(method_exists($this, $name)){
            $this->$name($value);
        }
        else{
            // Getter/Setter not defined so set as property of object
            $this->$name = $value;
        }
    }

    function __get($name){
        if(method_exists($this, $name)){
            return $this->$name();
        }
        elseif(property_exists($this,$name)){
            // Getter/Setter not defined so return property if it exists
            return $this->$name;
        }
        return null;
    }
}
