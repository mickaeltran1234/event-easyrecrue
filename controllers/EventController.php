<?php


namespace Controllers;

use DateTime;
use DateTimeZone;
use Models\Data;
use Models\Event;

require '../vendor/autoload.php';
class EventController
{

    public function __construct($post)
    {
            if (!empty($post)){
            foreach ($post as $key=>$infos){
                if ($key  != 'events' && !is_null($key)){
                    $tab[$key] = $infos;
                    $this->dataController = new Data($tab);
                }
                else if ($key  == 'events'){
                    $this->eventController = new Event($infos);
                    }
                }
            }
            else{
                echo 'post is empty';
            }
    }

    /*
     * Transform Timestamps to DateTime
     * FROM 1520357693
     * TO 2016-11-10 09:00:01
     */
    public function timestampsToDateTime($timestamps){
        $date = new DateTime();
        $date->setTimestamp($timestamps);
        $date->format('Y-m-d H:i:s');
        return $date;
    }


    /*
     * Validate format for one date
     * Param : string 'date' like start or end to refer at the object model Data
     * Format Y-m-d H:i:s
     * Change timeZone to object timeZone with format fr_FR to 'Europe/Paris'
     * Create object result with format Y-m-d H:i:s
     * Compare if result and result under format = $date
     * Return boolean or dateTime
     */
    public function validateDate($date, $format = 'Y-m-d H:i:s'){
            $dateFromPost = $this->dataController->__get($date);
            $tz = $this->validateTimeZone();
            if ($tz == false){
                return false;
            }else {
                $result = DateTime::createFromFormat($format, $dateFromPost , $tz);
                if ($result && $result->format($format) == $dateFromPost){
                    return $result;
                }else{
                    return $result && $result->format($format) == $dateFromPost;
                }
            }
        }

    /*
     * Get locale timezone
     * Verify regex (a to z) x2 '_' (A to Z) x2 stop
     * Use timeZone() to take the good Timezone
     * Create object DateTimeZone with parameter $tz from TimeZone()
     * return $tz
     */
    public function validateTimeZone(){
        $time = $this->dataController->__get('locale');
        if (preg_match("/^[a-z]{2}_[A-Z]{2}/",$time)){
            $timeZoneVal = $this->timeZone($time);
            $tz = new DateTimeZone($timeZoneVal);
            return $tz;
        }else{
            return false;
        }
    }
    /*
     * Change Timezone format
     * fr_FR to 'Europe/Paris'
     * Verify locale information
     * Return string
     */
    public function timeZone($time){
        $rfc = [
                'it_IT' => 'Europe/Rome',
                'fr_FR' => 'Europe/Paris',
                'fr_CA' => 'America/Toronto',
                'pt_PT' => 'Europe/Lisbon',
                'pt_BR' => 'America/Sao_Paulo'
        ];
        if (array_key_exists($time,$rfc)){
            return $rfc[$time];
        }else{
            return "Asia/Tokyo";
        }
    }

    /*
     * Check if start and end date are valid
     * Transform start, end to object dateTime if not valid return string
     * USE ALL TIMESTAMPS AND COMPARE -> for -> get(timestamps)
     * $date = Transform timestamps to date
     * Redefine timezone for $date because server on Europe/Berlin but we are testing with Locale variable
     * Compare object start >= date <= end
     * date to format Y-m-d H:i:s
     * Array [$key] = $dateOutput;
     * Looking if array is empty
     *  if not browse array
     *  else 'Nothing here for you'
     */
    public function search(){
        $start = $this->validateDate('start');
        $end = $this->validateDate('end');
        if ($start && $end){
            foreach ($this->eventController as $name=>$infos){
                $date = $this->eventController->__get($name);
                $date = $this->timestampsToDateTime(($date));
                $date->setTimezone($this->validateTimeZone());
                if ($start <= $date && $end >= $date) {
                    $dateOutput = $date->format('Y-m-d H:i:s');
                    $search[$name] = $dateOutput;
                }
            }
            if (!empty($search)){
                foreach ($search as $event => $date) {
                    echo $event . ' at ' . $date . '<br>';
                }
            }
            else{
                echo 'Nothing here for you';
            }
        }else{
            echo 'Nothing here for you';
        }
    }

    public function __toString()
    {
        return 'Event Controller Model';
    }
}
